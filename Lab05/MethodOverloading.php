<?php
class MethodTest {
	public function __call($name, $agruments) {
		echo "Calling object method '$name'". implode(',', $agruments) . "<br>";
	}
	public static function __callStatic($name, $agruments) {
		echo "Calling static method '$name' " .implode(',', $agruments) . "<br>";
	}
}
$obj = new MethodTest;
$obj->runTest('in object context');

MethodTest::runTest('in object context');

?>