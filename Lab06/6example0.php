 <html>
 <head>
 	<title>Exercise 0</title>
 </head>
 <body>  
 	<?php 
 	 $server = 'localhost'; 
 	 $user = 'root';  
 	 $pass = ''; 
 	 $mydb = 'mydatabase'; 
 	 $table_name1 = 'Businesses'; 
 	 $table_name2 = 'Categories';
 	 $table_name3 = 'Biz_Categories';
 	 $connect = mysqli_connect($server, $user, $pass); 
 	 if (!$connect) { 
 	  die ("Cannot connect to $server using $user"); }
 	 else { 
 	  $SQLcmd1 = "CREATE TABLE $table_name1(BusinessID INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,          
 	                                      Name VARCHAR(50), 
 	                                      Address VARCHAR(50),
 	                                      City VARCHAR(50), 
 	                                      Telephone VARCHAR(10),       
 	                                      URL VARCHAR(20))"; 
 	  $SQLcmd2 = "CREATE TABLE $table_name2(CategoryID INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
 	                                         Title VARCHAR(50), 
 	                                         Description VARCHAR(50))";
 	  $SQLcmd3 = "CREATE TABLE $table_name3(BusinessID INT UNSIGNED NOT NULL,
 	      	                                CategoryID INT UNSIGNED NOT NULL,
 	      	                                FOREIGN KEY (BusinessID) REFERENCES categories(BusinessID),
 	      	                                FOREIGN KEY (CategoryID) REFERENCES businesses(CategoryID))";
      mysqli_select_db($connect, $mydb); 
 	  if (mysqli_query($connect,$SQLcmd1)){ 
 	      print '<font size="4" color="blue" >Created Table'; 
 	      print "<i>$table_name1</i> in database <i>$mydb</i><br></font>";  
 	      print "<br>SQLcmd=$SQLcmd1"; 
         }else { 
 	      die ("Table Create Creation Failed SQLcmd=$SQLcmd1"); 
 	         } 
      if(mysqli_query($connect, $SQLcmd2)){
 	      	print '<font size="4" color="blue" >Created Table'; 
 	        print "<i>$table_name2</i> in database <i>$mydb</i><br></font>";  
 	        print "<br>SQLcmd=$SQLcmd2"; 
 	      }else {
 	      	die ("Table Create Creation Failed SQLcmd=$SQLcmd2");
 	      }
 	  if(mysqli_query($connect,$SQLcmd3)){
            	print '<font size="4" color="blue" >Created Table'; 
 	            print "<i>$table_name3</i> in database <i>$mydb</i><br></font>";  
 	            print "<br>SQLcmd=$SQLcmd3"; 
            }else {
            	die ("Table Create Creation Failed SQLcmd=$SQLcmd3");
            }
 	  mysqli_close($connect);
 	} 
?>
</body>
</html>